(function() {

    var statusBlock = $("#status");
    var messageContainer = $("#messages");
    var messageList = messageContainer.find('#list');
    var information = messageContainer.find('#information');



    var showMessages = function(response) {

        var status = "";
        var messages = [];

        if(!response.success) {
            status = "Error";
            messages = response.data.errors.map(function(er) {
                return er.Message;
            });

            messageContainer.addClass("alert-danger")
        } else {
            status = "Success";
            messages = response.data.messages.map(function(er) {
                return er.Message;
            });
            messageContainer.addClass("alert-success");
            information.removeClass('hidden');

            information.find('#link').find('.value').attr('href', response.data.information.link);

            information.find('#user').find('.value').text(response.data.information.user);
            information.find('#password').find('.value').text(response.data.information.password);
            information.find('#tracking').find('.value').text(response.data.information.tracking);
        }

        statusBlock.text(status);

        messages.map(function(message) {
            var el = document.createElement("li");
            el.innerText = message;
            messageList.append(el);
        });

        messageContainer.removeClass('hidden');
    };

    var hideMessages = function() {

        messageList.text('');
        statusBlock.text('');

        messageContainer.addClass('hidden');
        information.addClass('hidden');
        messageContainer.removeClass("alert-danger alert-success");

    };

    $("#form").on('submit', function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        // stub('', data, updateView)
        sendData($(this).attr('action'), data, updateView)
    });


    var sendData = function(url, data, cb) {
        return $.post(url, data, cb)
    };

    var stub = function(p1, p2, bc) {
        return setTimeout(function() {
            bc({
                success: true,
                data: {
                    information: {
                        link: "https://google.com"
                    }
                }
            })
        }, 1000)
    }

    var updateView = function(response) {
        hideMessages();
        if(response.success) {
            $('#form').addClass('hidden');
            $('#success').removeClass('hidden');
            $('.spinner').removeClass('hidden');
            var spinner = new Spinner({
                lines: 13 // The number of lines to draw
                , length: 56 // The length of each line
                , width: 10 // The line thickness
                , radius: 84 // The radius of the inner circle
                , scale: 0.5 // Scales overall size of the spinner
                , corners: 0 // Corner roundness (0..1)
                , color: '#000' // #rgb or #rrggbb or array of colors
                , opacity: 0.4 // Opacity of the lines
                , rotate: 40 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1 // Rounds per second
                , trail: 31 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: 'spinner' // The CSS class to assign to the spinner
                , top: '50%' // Top position relative to parent
                , left: '50%' // Left position relative to parent
                , shadow: false // Whether to render a shadow
                , hwaccel: false // Whether to use hardware acceleration
                , position: 'absolute' // Element positioning
            }).spin(document.querySelector('.spinner'));

            setTimeout(function() {
                // $('.spinner-wrapper').addClass('hidden')
                // $('.info-block').removeClass('hidden')
                // $('.fedex-link').removeClass('hidden')
                // $('#_link').attr('href', response.data.information.link);
                window.top.location.href = response.data.information.link;
            }, 15000);
            //window.top.location.href = response.data.information.link;
        } else {
            showMessages(response)
        }
    }
})();