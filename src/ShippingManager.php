<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:18 PM
 */

namespace FedexShipping;


use FedexShipping\Shipping\Request;

class ShippingManager {

    public function __construct($wsdl = false)
    {
        $this->path = $wsdl ? $wsdl : "./OpenShipService_v7.wsdl";
        $this->getSoapClient();
    }

    /** @var \SoapClient */
    protected $client;

    protected $latestResponse;

    /** @var string */
    protected $path;

    public function getSoapClient()
    {
        if($this->client) {
            return $this->client;
        } else {
            $this->client = new \SoapClient($this->path, ["trace" => 1]);

            return $this->client;
        }
    }

    public function sendRequest(Request $request, $production = true)
    {
        if($production) {
            $this->client->__setLocation("https://ws.fedex.com:443/web-services/openship");
        }
        $this->latestResponse = $this->client->createPendingShipment($request->getData());
        return $this->latestResponse;
    }

    public function saveLabels() {
        $fp = fopen(SHIP_CODLABEL, 'wb');
        fwrite($fp, $this->latestResponse->CompletedShipmentDetail->AssociatedShipments->Label->Parts->Image); //Create COD Return PNG or PDF file
        fclose($fp);

        $fp = fopen(SHIP_LABEL, 'wb');
        fwrite($fp, $this->latestResponse->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image); //Create PNG or PDF file
        fclose($fp);
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}