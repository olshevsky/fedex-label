<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:30 PM
 */

namespace FedexShipping\Shipping\Parts\ApiDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class TransactionDetail extends AbstractPart{

    public $partName = 'TransactionDetail';

    protected $info = [];

    protected $clientId;

    public function getData()
    {
        return array_merge($this->info, [
                "CustomerTransactionId" => $this->clientId,
            ]);
    }

    public static function create(array $data = [])
    {
        $transactionDetail = new TransactionDetail();
        $transactionDetail->setClientId(self::getValue($data, "CustomerTransactionId"));
        $transactionDetail->setInfo(self::getValue($data, "Information", []));

        return $transactionDetail;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param array $info
     * @return $this
     */
    public function setInfo($info)
    {
        $this->info = $info;
        return $this;
    }

}