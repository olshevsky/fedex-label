<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:30 PM
 */

namespace FedexShipping\Shipping\Parts\ApiDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class Version extends AbstractPart {

    public $partName = 'Version';

    protected $serviceId = "ship";

    protected $major = 15;

    protected $intermediate = 0;

    protected $minor = 0;

    public function getData()
    {
        return [
            'ServiceId' => $this->serviceId,
            'Major' => $this->major,
            'Intermediate' => $this->intermediate,
            'Minor' => $this->minor
        ];
    }

    public static function create($data = [])
    {
        $version = new Version();
        $version->setServiceId(self::getValue($data ,"ServiceId"));
        $version->setMajor(self::getValue($data ,"Major"));
        $version->setIntermediate(self::getValue($data ,"Intermediate"));
        $version->setMinor(self::getValue($data ,"Minor"));

        return $version;
    }

    /**
     * @return string
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param string $serviceId
     * @return $this
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMajor()
    {
        return $this->major;
    }

    /**
     * @param int $major
     * @return $this
     */
    public function setMajor($major)
    {
        $this->major = $major;
        return $this;
    }

    /**
     * @return int
     */
    public function getIntermediate()
    {
        return $this->intermediate;
    }

    /**
     * @param int $intermediate
     * @return $this
     */
    public function setIntermediate($intermediate)
    {
        $this->intermediate = $intermediate;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinor()
    {
        return $this->minor;
    }

    /**
     * @param int $minor
     * @return $this
     */
    public function setMinor($minor)
    {
        $this->minor = $minor;
        return $this;
    }
}