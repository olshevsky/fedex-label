<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:30 PM
 */

namespace FedexShipping\Shipping\Parts\ApiDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class ClientDetail extends AbstractPart {

    public $partName = 'ClientDetail';

    /** @var string */
    protected $meterNumber;

    /** @var string */
    protected $accountNumber;

    public function getData()
    {
        return [
            "AccountNumber" => $this->accountNumber,
            "MeterNumber" => $this->meterNumber,
        ];
    }

    public static function create($data = [])
    {
        $clientDetail = new ClientDetail();
        $clientDetail->setAccountNumber(self::getValue($data, "AccountNumber"));
        $clientDetail->setMeterNumber(self::getValue($data, "MeterNumber"));

        return $clientDetail;
    }

    /**
     * @return string
     */
    public function getMeterNumber()
    {
        return $this->meterNumber;
    }

    /**
     * @param string $meterNumber
     * @return $this
     */
    public function setMeterNumber($meterNumber)
    {
        $this->meterNumber = $meterNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     * @return $this
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }
}