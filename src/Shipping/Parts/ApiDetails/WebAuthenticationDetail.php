<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:30 PM
 */

namespace FedexShipping\Shipping\Parts\ApiDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class WebAuthenticationDetail extends AbstractPart {

    public $partName = 'WebAuthenticationDetail';

    protected $key;

    protected $password;

    public function getData()
    {
        return [
            "UserCredential" => [
                "Key" => $this->key,
                "Password" => $this->password,
            ]
        ];
    }

    public static function create($data = [])
    {
        $authDetail = new WebAuthenticationDetail();
        $authDetail->setKey(self::getValue($data, "Key"));
        $authDetail->setPassword(self::getValue($data, "Password"));

        return $authDetail;
    }

    /**
     * @return string
     */
    public function getPartName()
    {
        return $this->partName;
    }

    /**
     * @param string $partName
     * @return $this
     */
    public function setPartName($partName)
    {
        $this->partName = $partName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}