<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 7:28 PM
 */

namespace FedexShipping\Shipping\Parts;


abstract class AbstractPart {

    public $partName = false;

    /**
     * @return array
     */
    public function getData()
    {
        return [];
    }

    public static function create($data = []) { return $data; }

    public static function getValue($array, $index, $defaultValue = null)
    {
        if(isset($array[$index])) {
            return $array[$index];
        }

        return $defaultValue;
    }
}