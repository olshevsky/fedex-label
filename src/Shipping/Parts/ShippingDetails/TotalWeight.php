<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:32 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class TotalWeight extends AbstractPart {

    public $partName = "TotalWeight";

    const LB = "LB";
    const KG = "KG";

    protected $val = 0.0;

    protected $unit = self::LB;

    public function getData()
    {
        return [
            'Value' => $this->val,
            'Units' => $this->unit,
        ];
    }

    public static function create($data = [])
    {
        $totalWeight = new TotalWeight();
        $totalWeight->setUnit(self::getValue($data, "Unit", self::KG));
        $totalWeight->setVal(self::getValue($data, "Value", 0));

        return $totalWeight;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return int
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * @param int $val
     * @return $this
     */
    public function setVal($val)
    {
        $this->val = $val;
        return $this;
    }
}