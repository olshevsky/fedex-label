<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/10/15
 * Time: 12:59 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class PendingShipmentDetail extends AbstractPart {

    public $partName = "PendingShipmentDetail";

    const TYPE_EMAIL = "EMAIL";

    protected $expirationDate;

    protected $type = self::TYPE_EMAIL;

    protected $message = '';

    /** @var  RecipientsSet */
    protected $recipients;

    public function getData()
    {
        return [
            'Type' => 'EMAIL',
            'ExpirationDate' => $this->expirationDate,
            'EmailLabelDetail' => array(
                'Recipients' => $this->getRecipients()->getData(),
            )
        ];
    }

    public static function create($data = [])
    {
        $pendingShipmentDetail = new PendingShipmentDetail();

        $pendingShipmentDetail->setMessage(self::getValue($data, "Message", ''));
        $pendingShipmentDetail->setRecipients(self::getValue($data, "Recipients", RecipientsSet::create()));

        $pendingShipmentDetail->setExpirationDate(self::getValue($data, "ExpirationDate",
            date("Y-m-d", mktime(8, 0, 0, date("m"), date("d")+5, date("Y"))))
        );

        $pendingShipmentDetail->setType(self::getValue($data, "Type", self::TYPE_EMAIL));

        return $pendingShipmentDetail;
    }

    /**
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param mixed $expirationDate
     * @return $this
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param array $recipients
     * @return $this
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}