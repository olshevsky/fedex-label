<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:33 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class PackageLineItem extends AbstractPart {
    protected $sequenceNumber = 0;
    protected $groupPackageCount = 0;

    /** @var  TotalWeight */
    protected $weight;

    /** @var  Dimensions */
    protected $dimensions;

    /** @var  string */
    protected $description;

    public function getData()
    {
        return [
            "SequenceNumber" => $this->sequenceNumber,
            "GroupPackageCount" => $this->groupPackageCount,
            "Weight" => $this->weight->getData(),
            "Dimensions" => $this->dimensions->getData(),
            "ItemDescription" => $this->description,
        ];
    }

    public static function create($data = [])
    {
        $item = new PackageLineItem();
        $item->setSequenceNumber(self::getValue($data ,"SequenceNumber", 0));
        $item->setGroupPackageCount(self::getValue($data ,"GroupPackageCount", 0));
        $item->setWeight(self::getValue($data ,"Weight", new TotalWeight()));
        $item->setDimensions(self::getValue($data ,"Dimensions", new Dimensions()));
        $item->setDescription(self::getValue($data ,"ItemDescription", ''));

        return $item;
    }

    /**
     * @return int
     */
    public function getSequenceNumber()
    {
        return $this->sequenceNumber;
    }

    /**
     * @param int $sequenceNumber
     * @return $this
     */
    public function setSequenceNumber($sequenceNumber)
    {
        $this->sequenceNumber = $sequenceNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupPackageCount()
    {
        return $this->groupPackageCount;
    }

    /**
     * @param int $groupPackageCount
     * @return $this
     */
    public function setGroupPackageCount($groupPackageCount)
    {
        $this->groupPackageCount = $groupPackageCount;
        return $this;
    }

    /**
     * @return TotalWeight
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param TotalWeight $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return Dimensions
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param Dimensions $dimensions
     * @return $this
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

}