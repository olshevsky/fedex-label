<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/10/15
 * Time: 1:24 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class ShipmentRecipient extends AbstractPart {

    const SHIPMENT_COMPLETOR = "SHIPMENT_COMPLETOR";
    const SHIPMENT_INITIATOR = "SHIPMENT_INITIATOR";

    protected $emailAddress;
    protected $role = self::SHIPMENT_COMPLETOR;

    public function getData() {
        return [
            'EmailAddress' => $this->emailAddress,
            'Role' => $this->role
        ];
    }

    public static function create($data = [])
    {
        $shipmentRecipient = new ShipmentRecipient();
        $shipmentRecipient->setEmailAddress(self::getValue($data, 'EmailAddress', ''));
        $shipmentRecipient->setRole(self::getValue($data, 'Role', self::SHIPMENT_COMPLETOR));

        return $shipmentRecipient;
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param mixed $emailAddress
     * @return $this
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
}