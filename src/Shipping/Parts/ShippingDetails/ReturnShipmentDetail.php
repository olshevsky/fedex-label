<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/10/15
 * Time: 12:58 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class ReturnShipmentDetail extends AbstractPart {

    public $partName = "ReturnShipmentDetail";

    const RETURN_TYPE = "RETURN";
    const SPECIAL_SERVICES_SATURDAY_DELIVERY = "SATURDAY_DELIVERY";

    protected $type = self::RETURN_TYPE;

    protected $merchantPhoneNumber;

    protected $specialServices;

    public function getData()
    {
        return [
            'ReturnType' => $this->type,
            'ReturnEMailDetail' => [
                'MerchantPhoneNumber' => $this->merchantPhoneNumber
            ]
        ];
    }

    public static function create($data = [])
    {
        $returnShipmentDetail = new ReturnShipmentDetail();

        $returnShipmentDetail->setMerchantPhoneNumber(self::getValue($data, "MerchantPhoneNumber", ''));
        $returnShipmentDetail->setType(self::getValue($data, "MerchantPhoneNumber", self::RETURN_TYPE));

        return $returnShipmentDetail;
    }

    /**
     * @return mixed
     */
    public function getMerchantPhoneNumber()
    {
        return $this->merchantPhoneNumber;
    }

    /**
     * @param mixed $merchantPhoneNumber
     * @return $this
     */
    public function setMerchantPhoneNumber($merchantPhoneNumber)
    {
        $this->merchantPhoneNumber = $merchantPhoneNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialServices()
    {
        return $this->specialServices;
    }

    /**
     * @param mixed $specialServices
     * @return $this
     */
    public function setSpecialServices($specialServices)
    {
        $this->specialServices = $specialServices;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}