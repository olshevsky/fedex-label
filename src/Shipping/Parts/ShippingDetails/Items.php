<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/5/15
 * Time: 11:49 AM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class Items extends AbstractPart {

    public $partName = "RequestedPackageLineItems";

    /**
     * @var PackageLineItem[]
     */
    protected $items = [];

    public function getData()
    {
        $items = [];
        foreach ($this->items as $key => $item) {
            $items[$key] = $item->getData();
        }
        return $items;
    }

    public static function create($data = [])
    {
        $itemsContainer = new Items();
        $itemsContainer->setItems($data);

        return $itemsContainer;
    }

    /**
     * @return PackageLineItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param PackageLineItem[] $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }
}