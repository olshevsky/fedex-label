<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 8:19 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class Contact extends AbstractPart {

    protected $personName;
    protected $companyName;
    protected $phoneNumber;

    public function getData() {
        return [
            'PersonName' => $this->personName,
            'CompanyName' => $this->companyName,
            'PhoneNumber' => $this->phoneNumber
        ];
    }

    public static function create($data = [])
    {
        $contact = new Contact();
        $contact->setCompanyName(self::getValue($data ,"CompanyName"));
        $contact->setPersonName(self::getValue($data ,"PersonName"));
        $contact->setPhoneNumber(self::getValue($data ,"PhoneNumber"));

        return $contact;
    }

    /**
     * @return mixed
     */
    public function getPersonName()
    {
        return $this->personName;
    }

    /**
     * @param mixed $personName
     * @return $this
     */
    public function setPersonName($personName)
    {
        $this->personName = $personName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }
}