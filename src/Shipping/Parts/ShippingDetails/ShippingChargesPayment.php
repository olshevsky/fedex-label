<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:32 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class ShippingChargesPayment extends AbstractPart {
    public $partName = "ShippingChargesPayment";

    /** @var  Payor */
    protected $payor;

    const SENDER = "SENDER";

    protected $paymentType = "SENDER";

    public function getData()
    {
        return [
            "PaymentType" => $this->paymentType,
            "Payor" => $this->payor->getData()
        ];
    }

    public static function create($data = [])
    {
        $shippingChargesPayment = new ShippingChargesPayment();
        $shippingChargesPayment->setPayor(self::getValue($data, "Payor", Payor::create([])));
        $shippingChargesPayment->setPaymentType(self::getValue($data, "PaymentType", self::SENDER));
        return $shippingChargesPayment;
    }

    /**
     * @return Payor
     */
    public function getPayor()
    {
        return $this->payor;
    }

    /**
     * @param Payor $payor
     * @return $this
     */
    public function setPayor($payor)
    {
        $this->payor = $payor;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     * @return $this
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
        return $this;
    }
}