<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 8:48 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class ResponsibleParty extends AbstractPart {

    protected $accountNumber;

    /** @var  Contact */
    protected $contact;

    /** @var  Address */
    protected $address;

    public function getData()
    {
        return [
            "AccountNumber" => $this->accountNumber,
            "Contact" => $this->contact->getData(),
            "Address" => $this->address->getData()
        ];
    }

    public static function create($data = [])
    {
        $responsibleParty = new ResponsibleParty();
        $responsibleParty->setAccountNumber(self::getValue($data, "AccountNumber"));

        $responsibleParty->setContact(self::getValue($data, "Contact", new Contact()));
        $responsibleParty->setAddress(self::getValue($data, "Address", new Address()));

        return $responsibleParty;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param mixed $accountNumber
     * @return $this
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }
}