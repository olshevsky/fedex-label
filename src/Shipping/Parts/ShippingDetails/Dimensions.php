<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/5/15
 * Time: 11:34 AM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class Dimensions extends AbstractPart {
    const INCHES = "IN";

    protected $length = 0;
    protected $width = 0;
    protected $height = 0;
    protected $units = self::INCHES;

    public function getData()
    {
        return [
            'Length' => $this->length,
            'Width' => $this->width,
            'Height' => $this->height,
            'Units' => $this->units,
        ];
    }

    public static function create($data = [])
    {
        $dimensions = new Dimensions();
        $dimensions->setLength(self::getValue($data ,"Length", 0));
        $dimensions->setWidth(self::getValue($data ,"Width", 0));
        $dimensions->setHeight(self::getValue($data ,"Height", 0));
        $dimensions->setUnits(self::getValue($data ,"Units", self::INCHES));

        return $dimensions;
    }

    /**
     * @return boolean
     */
    public function isPartName()
    {
        return $this->partName;
    }

    /**
     * @param boolean $partName
     * @return $this
     */
    public function setPartName($partName)
    {
        $this->partName = $partName;
        return $this;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return $this
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param string $units
     * @return $this
     */
    public function setUnits($units)
    {
        $this->units = $units;
        return $this;
    }

}