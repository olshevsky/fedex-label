<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/10/15
 * Time: 1:24 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class RecipientsSet extends AbstractPart {

    protected $recipients = [];

    public function getData()
    {
        return array_map(function($recipient) {
            if($recipient instanceof AbstractPart) {
                return $recipient->getData();
            }

            return null;
        }, $this->recipients);
    }

    public static function create($data = [])
    {
        $recipientsSet = new RecipientsSet();
        $recipientsSet->setRecipients($data);
        return $recipientsSet;
    }

    /**
     * @return array
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param array $recipients
     * @return $this
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
        return $this;
    }
}