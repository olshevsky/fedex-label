<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:31 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class DropoffType extends AbstractPart {

    public $partName = "DropoffType";

    const REGULAR_PICKUP = "REGULAR_PICKUP";
    const REQUEST_COURIER = "REQUEST_COURIER";
    const DROP_BOX = "DROP_BOX";
    const BUSINESS_SERVICE_CENTER = "BUSINESS_SERVICE_CENTER";
    const STATION = "STATION";

    protected $type = self::REGULAR_PICKUP;

    public function getData()
    {
        return $this->type;
    }

    public static function create($type = false)
    {
        $type = $type ? $type : self::REGULAR_PICKUP;

        $dropType = new DropoffType();
        $dropType->setType($type);

        return $dropType;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}