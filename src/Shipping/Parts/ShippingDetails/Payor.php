<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 8:46 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class Payor extends AbstractPart {

    public $partName = "Payor";

    /** @var  ResponsibleParty */
    protected $responsibleParty;

    public function getData()
    {
        return [
            "ResponsibleParty" => $this->responsibleParty->getData()
        ];
    }

    public static function create($data = [])
    {
        $payor = new Payor();
        $payor->setResponsibleParty(self::getValue($data, "ResponsibleParty", ResponsibleParty::create([])));

        return $payor;
    }

    /**
     * @return ResponsibleParty
     */
    public function getResponsibleParty()
    {
        return $this->responsibleParty;
    }

    /**
     * @param ResponsibleParty $responsibleParty
     * @return $this
     */
    public function setResponsibleParty($responsibleParty)
    {
        $this->responsibleParty = $responsibleParty;
        return $this;
    }
}