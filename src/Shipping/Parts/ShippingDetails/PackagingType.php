<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:32 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class PackagingType extends AbstractPart {

    public $partName = "PackagingType";

    const YOUR_PACKAGING = "YOUR_PACKAGING";
    const FEDEX_BOX = "FEDEX_BOX";
    const FEDEX_PAK = "FEDEX_PAK";
    const FEDEX_TUBE = "FEDEX_TUBE";

    protected $type = self::YOUR_PACKAGING;

    public function getData()
    {
        return $this->type;
    }

    public static function create($type = false)
    {
        $type = $type ? $type : self::YOUR_PACKAGING;

        $packagingType = new PackagingType();
        $packagingType->setType($type);

        return $packagingType;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}