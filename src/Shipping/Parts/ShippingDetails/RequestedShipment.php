<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:31 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class RequestedShipment extends AbstractPart {

    /** @var  DropoffType */
    protected $dropOffType;

    /** @var  ShipmentTimestamp */
    protected $shipTimestamp;

    /** @var  ServiceType */
    protected $serviceType;

    /** @var  PackagingType */
    protected $packagingType;

    /** @var  TotalWeight */
    protected $totalWeight;

    /** @var  Shipper */
    protected $shipper;

    /** @var  Recipient */
    protected $recipient;

    /** @var  ShippingChargesPayment */
    protected $shippingChargesPayment;

    /** @var  SpecialServicesRequested */
    protected $specialServicesRequested;

    /** @var  LabelSpecification */
    protected $labelSpecification;

    protected $packageCount = 1;

    /** @var  Items */
    protected $items;

    public function getData()
    {
        return [
            "ShipTimestamp" => $this->shipTimestamp->getData(),
            "DropoffType" => $this->dropOffType->getData(),
            "ServiceType" => $this->serviceType->getData(),
            "PackagingType" => $this->packagingType->getData(),
            "Shipper" => $this->shipper->getData(),
            "Recipient" => $this->recipient->getData(),
            "ShippingChargesPayment" => $this->shippingChargesPayment->getData(),
            "SpecialServicesRequested" => $this->specialServicesRequested->getData(),
            "LabelSpecification" => $this->labelSpecification->getData(),
            "RequestedPackageLineItems" => $this->items->getData(),
        ];
    }

    /**
     * @return DropoffType
     */
    public function getDropOffType()
    {
        return $this->dropOffType;
    }

    /**
     * @param DropoffType $dropOffType
     * @return $this
     */
    public function setDropOffType($dropOffType)
    {
        $this->dropOffType = $dropOffType;
        return $this;
    }

    /**
     * @return ShipmentTimestamp
     */
    public function getShipTimestamp()
    {
        return $this->shipTimestamp;
    }

    /**
     * @param ShipmentTimestamp $shipTimestamp
     * @return $this
     */
    public function setShipTimestamp($shipTimestamp)
    {
        $this->shipTimestamp = $shipTimestamp;
        return $this;
    }

    /**
     * @return ServiceType
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @param ServiceType $serviceType
     * @return $this
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;
        return $this;
    }

    /**
     * @return PackagingType
     */
    public function getPackagingType()
    {
        return $this->packagingType;
    }

    /**
     * @param PackagingType $packagingType
     * @return $this
     */
    public function setPackagingType($packagingType)
    {
        $this->packagingType = $packagingType;
        return $this;
    }

    /**
     * @return TotalWeight
     */
    public function getTotalWeight()
    {
        return $this->totalWeight;
    }

    /**
     * @param TotalWeight $totalWeight
     * @return $this
     */
    public function setTotalWeight($totalWeight)
    {
        $this->totalWeight = $totalWeight;
        return $this;
    }

    /**
     * @return Shipper
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param Shipper $shipper
     * @return $this
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
        return $this;
    }

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     * @return $this
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return ShippingChargesPayment
     */
    public function getShippingChargesPayment()
    {
        return $this->shippingChargesPayment;
    }

    /**
     * @param ShippingChargesPayment $shippingChargesPayment
     * @return $this
     */
    public function setShippingChargesPayment($shippingChargesPayment)
    {
        $this->shippingChargesPayment = $shippingChargesPayment;
        return $this;
    }

    /**
     * @return SpecialServicesRequested
     */
    public function getSpecialServicesRequested()
    {
        return $this->specialServicesRequested;
    }

    /**
     * @param SpecialServicesRequested $specialServicesRequested
     * @return $this
     */
    public function setSpecialServicesRequested($specialServicesRequested)
    {
        $this->specialServicesRequested = $specialServicesRequested;
        return $this;
    }

    /**
     * @return LabelSpecification
     */
    public function getLabelSpecification()
    {
        return $this->labelSpecification;
    }

    /**
     * @param LabelSpecification $labelSpecification
     * @return $this
     */
    public function setLabelSpecification($labelSpecification)
    {
        $this->labelSpecification = $labelSpecification;
        return $this;
    }

    /**
     * @return int
     */
    public function getPackageCount()
    {
        return $this->packageCount;
    }

    /**
     * @param int $packageCount
     * @return $this
     */
    public function setPackageCount($packageCount)
    {
        $this->packageCount = $packageCount;
        return $this;
    }

    /**
     * @return Items
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Items $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }
}