<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:33 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class LabelSpecification extends AbstractPart {

    public $partName = "LabelSpecification";

    const COMMON2D_TYPE = "COMMON2D";
    const LABEL_DATA_ONLY = "LABEL_DATA_ONLY";

    const PDF = "PDF";
    const DPL = "DPL";
    const EPL2 = "EPL2";
    const ZPLII = "ZPLII";
    const PNG = "PNG";

    const STOCK_TYPE= "PAPER_7X4.75";

    protected $labelFormatType = self::COMMON2D_TYPE;
    protected $imageType = self::PDF;
    protected $labelStockType = self::STOCK_TYPE;

    public static function create()
    {
        $labelSpecification = new LabelSpecification();

        return $labelSpecification;
    }

    public function getData()
    {
        return [
            "LabelFormatType" => $this->labelFormatType,
            "ImageType" => $this->imageType,
            "LabelStockType" => $this->labelStockType,
        ];
    }
}