<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:32 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class SpecialServicesRequested extends AbstractPart {

    public $partName = "SpecialServicesRequested";

    /** @var  ReturnShipmentDetail */
    protected $returnShipmentDetail;

    /** @var  PendingShipmentDetail */
    protected $pendingShipmentDetail;

    public function getData()
    {
        return [
            "SpecialServiceTypes" => ['PENDING_SHIPMENT'],
            "PendingShipmentDetail" => $this->pendingShipmentDetail->getData(),
        ];
    }

    public static function create($data = []) {
        $specialServiceRequested = new SpecialServicesRequested();
        $specialServiceRequested->setPendingShipmentDetail(self::getValue($data, "PendingShipmentDetail", PendingShipmentDetail::create()));

        return $specialServiceRequested;
    }

    /**
     * @return ReturnShipmentDetail
     */
    public function getReturnShipmentDetail()
    {
        return $this->returnShipmentDetail;
    }

    /**
     * @param ReturnShipmentDetail $returnShipmentDetail
     * @return $this
     */
    public function setReturnShipmentDetail($returnShipmentDetail)
    {
        $this->returnShipmentDetail = $returnShipmentDetail;
        return $this;
    }

    /**
     * @return PendingShipmentDetail
     */
    public function getPendingShipmentDetail()
    {
        return $this->pendingShipmentDetail;
    }

    /**
     * @param PendingShipmentDetail $pendingShipmentDetail
     * @return $this
     */
    public function setPendingShipmentDetail($pendingShipmentDetail)
    {
        $this->pendingShipmentDetail = $pendingShipmentDetail;
        return $this;
    }
}