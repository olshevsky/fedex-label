<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:32 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class Recipient extends AbstractPart {
    public $partName = "Recipient";

    /** @var Contact */
    protected $contact;

    /** @var Address */
    protected $address;

    public function getData()
    {
        return [
            "Contact" => $this->contact->getData(),
            "Address" => $this->address->getData()
        ];
    }

    public static function create($data = [])
    {
        $shipper = new Recipient();
        $shipper->setAddress(self::getValue($data, "Address", new Address()));
        $shipper->setContact(self::getValue($data, "Contact", new Contact()));

        return $shipper;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }
}