<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:31 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class ShipmentTimestamp extends AbstractPart {

    public $partName = "ShipTimestamp";

    protected $timestamp;

    public function getData()
    {
        return $this->timestamp;
    }

    public static function create($timestamp = false)
    {
        $timestamp = $timestamp ? $timestamp : date('c');

        $shipmentTimestamp = new ShipmentTimestamp();
        $shipmentTimestamp->setTimestamp($timestamp);

        return $shipmentTimestamp;
    }

    /**
     * @return bool|string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param bool|string $timestamp
     * @return $this
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

}