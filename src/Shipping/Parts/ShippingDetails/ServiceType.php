<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:31 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;

use FedexShipping\Shipping\Parts\AbstractPart;

class ServiceType extends AbstractPart {

    public $partName = "ServiceType";

    const PRIORITY_OVERNIGHT = "PRIORITY_OVERNIGHT";
    const STANDARD_OVERNIGHT = "STANDARD_OVERNIGHT";
    const FEDEX_GROUND = "FEDEX_GROUND";
    const FEDEX_EXPRESS_SAVER = "FEDEX_EXPRESS_SAVER";

    protected $type = self::FEDEX_GROUND;

    public function getData()
    {
        return $this->type;
    }

    public static function create($type = false)
    {
        $type = $type ? $type : self::FEDEX_GROUND;
        $serviceType = new ServiceType();
        $serviceType->setType($type);

        return $serviceType;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}