<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 8:19 PM
 */

namespace FedexShipping\Shipping\Parts\ShippingDetails;


use FedexShipping\Shipping\Parts\AbstractPart;

class Address extends AbstractPart {

    protected $streetLines = [];
    protected $city;
    protected $stateOrProvinceCode;
    protected $postalCode;
    protected $countryCode;
    protected $residential = false;

    public function getData() {
        return [
            'StreetLines' => $this->getStreetLines(),
            'City' => $this->getCity(),
            'StateOrProvinceCode' => $this->getStateOrProvinceCode(),
            'PostalCode' => $this->getPostalCode(),
            'CountryCode' => $this->getCountryCode(),
            'Residential' => $this->getResidential(),
        ];
    }

    public static function create($data = [])
    {
        $address = new Address();
        $address->setStreetLines(self::getValue($data ,'StreetLines', []));
        $address->setCity(self::getValue($data ,"City"));
        $address->setStateOrProvinceCode(self::getValue($data ,'StateOrProvinceCode'));
        $address->setPostalCode(self::getValue($data ,'PostalCode'));
        $address->setCountryCode(self::getValue($data ,'CountryCode'));
        $address->setResidential(self::getValue($data ,'Residential'));

        return $address;
    }

    /**
     * @return array
     */
    public function getStreetLines()
    {
        return $this->streetLines;
    }

    /**
     * @param array $streetLines
     * @return $this
     */
    public function setStreetLines($streetLines)
    {
        $this->streetLines = $streetLines;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStateOrProvinceCode()
    {
        return $this->stateOrProvinceCode;
    }

    /**
     * @param mixed $stateOrProvinceCode
     * @return $this
     */
    public function setStateOrProvinceCode($stateOrProvinceCode)
    {
        $this->stateOrProvinceCode = $stateOrProvinceCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getResidential()
    {
        return $this->residential;
    }

    /**
     * @param boolean $residential
     * @return $this
     */
    public function setResidential($residential)
    {
        $this->residential = $residential;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     * @return $this
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

}