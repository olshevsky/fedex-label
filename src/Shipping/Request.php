<?php
/**
 * Created by PhpStorm.
 * User: igor.olshevsky
 * Date: 6/4/15
 * Time: 6:25 PM
 */

namespace FedexShipping\Shipping;

use FedexShipping\Shipping\Parts\AbstractPart;
use FedexShipping\Shipping\Parts\ApiDetails\ClientDetail;
use FedexShipping\Shipping\Parts\ApiDetails\TransactionDetail;
use FedexShipping\Shipping\Parts\ApiDetails\Version;
use FedexShipping\Shipping\Parts\ApiDetails\WebAuthenticationDetail;
use FedexShipping\Shipping\Parts\ShippingDetails\RequestedShipment;
use FedexShipping\Shipping\Parts\ShippingDetails\TotalWeight;

class Request {

    /** @var  WebAuthenticationDetail */
    protected $WebAuthenticationDetail;

    /** @var  ClientDetail */
    protected $ClientDetail;

    /** @var  TransactionDetail */
    protected $TransactionDetail;

    /** @var  Version */
    protected $Version;

    /** @var  RequestedShipment */
    protected $RequestedShipment;

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        foreach(get_object_vars($this) as $key => $part) {
            if($part instanceof AbstractPart) {
                $data[$key] = $part->getData();
            }
        }

        $data["Actions"] = ["TRANSFER"];

        return $data;
    }

    /**
     * @return WebAuthenticationDetail
     */
    public function getWebAuthenticationDetail()
    {
        return $this->WebAuthenticationDetail;
    }

    /**
     * @param WebAuthenticationDetail $WebAuthenticationDetail
     * @return $this
     */
    public function setWebAuthenticationDetail($WebAuthenticationDetail)
    {
        $this->WebAuthenticationDetail = $WebAuthenticationDetail;
        return $this;
    }

    /**
     * @return ClientDetail
     */
    public function getClientDetail()
    {
        return $this->ClientDetail;
    }

    /**
     * @param ClientDetail $ClientDetail
     * @return $this
     */
    public function setClientDetail($ClientDetail)
    {
        $this->ClientDetail = $ClientDetail;
        return $this;
    }

    /**
     * @return TransactionDetail
     */
    public function getTransactionDetail()
    {
        return $this->TransactionDetail;
    }

    /**
     * @param TransactionDetail $TransactionDetail
     * @return $this
     */
    public function setTransactionDetail($TransactionDetail)
    {
        $this->TransactionDetail = $TransactionDetail;
        return $this;
    }

    /**
     * @return Version
     */
    public function getVersion()
    {
        return $this->Version;
    }

    /**
     * @param Version $Version
     * @return $this
     */
    public function setVersion($Version)
    {
        $this->Version = $Version;
        return $this;
    }

    /**
     * @return RequestedShipment
     */
    public function getRequestedShipment()
    {
        return $this->RequestedShipment;
    }

    /**
     * @param RequestedShipment $RequestedShipment
     * @return $this
     */
    public function setRequestedShipment($RequestedShipment)
    {
        $this->RequestedShipment = $RequestedShipment;
        return $this;
    }
}