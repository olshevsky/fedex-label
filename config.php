<?php
namespace Application;

use FedexShipping\Shipping\Parts\ShippingDetails\Address;
use FedexShipping\Shipping\Parts\ShippingDetails\Contact;
use FedexShipping\Shipping\Parts\ShippingDetails\Dimensions;
use FedexShipping\Shipping\Parts\ShippingDetails\TotalWeight;

/**
 * Key                 Prod: wq33o94NfqVwxl1h                |     Test: L1y1w8jkTFOmiSao
 * Password            Prod: zsTxDfc5adwAbRBK1chHYLjzq       |     Test: vjWLs02C2dBgLJATswOQT3iDx
 * AccountNumber       Prod: 464720383                       |     Test: 510087828
 * MeterNumber         Prod: 108050719                       |     Test: 118680445
 */

/**
 *
 * Supported Web Services:	 FedEx Web Services for Shipping
 * Authentication Key:	 wq33o94NfqVwxl1h
 * Meter Number:	 108050719
 *
 */

return [
    "Key" => "wq33o94NfqVwxl1h",
    "Password" => "zsTxDfc5adwAbRBK1chHYLjzq",
    "AccountNumber" => "464720383",
    "MeterNumber" => "108050719",

    "PackageDetails" => [
        'SequenceNumber'=> 1,
        'GroupPackageCount'=> 1,

        'Weight' => [
            'Value' => 1.0,
            'Unit' => TotalWeight::LB
        ],
        'Dimensions' => [
            'Length' => 6,
            'Width' => 6,
            'Height' => 4,
            'Units' => Dimensions::INCHES
        ]
    ],

    "Recipient" => [
        "Contact" => [
            'PersonName' => 'BYF Dental Enterprise',
            'CompanyName' => 'BYF Dental Enterprise',
            'PhoneNumber' => '8003611659'
        ],
        "Address" => [
            'StreetLines' => ['2526 Qume Dr. STE 15'],
            'City' => 'San Jose',
            'StateOrProvinceCode' => 'CA',
            'PostalCode' => '95131',
            'CountryCode' => 'US'
        ]
    ],

    "Payor" => [
        'AccountNumber' => "464720383",
        'Contact' => Contact::create([]),
        'Address' => Address::create([
            'CountryCode' => 'US'
        ]),
    ],

    "TotalWeight" => [
        "Value" => 1.0,
        "Unit" => TotalWeight::LB
    ],
];