<?php
    $states = [
        'AL'=>'ALABAMA',
        'AK'=>'ALASKA',
        'AS'=>'AMERICAN SAMOA',
        'AZ'=>'ARIZONA',
        'AR'=>'ARKANSAS',
        'CA'=>'CALIFORNIA',
        'CO'=>'COLORADO',
        'CT'=>'CONNECTICUT',
        'DE'=>'DELAWARE',
        'DC'=>'DISTRICT OF COLUMBIA',
        'FM'=>'FEDERATED STATES OF MICRONESIA',
        'FL'=>'FLORIDA',
        'GA'=>'GEORGIA',
        'GU'=>'GUAM GU',
        'HI'=>'HAWAII',
        'ID'=>'IDAHO',
        'IL'=>'ILLINOIS',
        'IN'=>'INDIANA',
        'IA'=>'IOWA',
        'KS'=>'KANSAS',
        'KY'=>'KENTUCKY',
        'LA'=>'LOUISIANA',
        'ME'=>'MAINE',
        'MH'=>'MARSHALL ISLANDS',
        'MD'=>'MARYLAND',
        'MA'=>'MASSACHUSETTS',
        'MI'=>'MICHIGAN',
        'MN'=>'MINNESOTA',
        'MS'=>'MISSISSIPPI',
        'MO'=>'MISSOURI',
        'MT'=>'MONTANA',
        'NE'=>'NEBRASKA',
        'NV'=>'NEVADA',
        'NH'=>'NEW HAMPSHIRE',
        'NJ'=>'NEW JERSEY',
        'NM'=>'NEW MEXICO',
        'NY'=>'NEW YORK',
        'NC'=>'NORTH CAROLINA',
        'ND'=>'NORTH DAKOTA',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'OH'=>'OHIO',
        'OK'=>'OKLAHOMA',
        'OR'=>'OREGON',
        'PW'=>'PALAU',
        'PA'=>'PENNSYLVANIA',
        'PR'=>'PUERTO RICO',
        'RI'=>'RHODE ISLAND',
        'SC'=>'SOUTH CAROLINA',
        'SD'=>'SOUTH DAKOTA',
        'TN'=>'TENNESSEE',
        'TX'=>'TEXAS',
        'UT'=>'UTAH',
        'VT'=>'VERMONT',
        'VI'=>'VIRGIN ISLANDS',
        'VA'=>'VIRGINIA',
        'WA'=>'WASHINGTON',
        'WV'=>'WEST VIRGINIA',
        'WI'=>'WISCONSIN',
        'WY'=>'WYOMING',
        'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
        'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
        'AP'=>'ARMED FORCES PACIFIC'
    ];
?>
<html>
<head>
    <title>Email Labels</title>
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//fgnass.github.io/spin.js/spin.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <style>
        #success {
            width: 100%;
            height: 100%;
            align-self: center;
            justify-content: center;
            align-items: center;
            align-content: center;
            display: flex;
            text-align: center;
        }
        .spinner {
            position: relative;
            height: 200px;
        }
    </style>
</head>
<body>

    <div class="form">
        <form method="post" id="form" class="container-fluid" action="./create_label.php">
            <div class="form-group">
                <label for="contactName">Contact Name <sup>*</sup></label>
                <input type="text" id="contactName" class="form-control" name="contactName" placeholder="Contact Name">
            </div>
            <div class="form-group">
                <label for="company">Company</label>
                <input type="text" class="form-control" id="company" name="company" placeholder="Company">
            </div>
            <div class="form-group">
                <label for="address1">Address 1 <sup>*</sup></label>
                <input type="text" class="form-control" id="address1" name="address[]" placeholder="Address 1">
            </div>
            <div class="form-group">
                <label for="address2">Address 2</label>
                <input class="form-control" id="address2" name="address[]" placeholder="Address 2">
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city" placeholder="City">
            </div>
            <div class="form-group">
                <label for="state">State <sup>*</sup></label>
                <select name="state" class="form-control" id="state">
                    <?php foreach($states as $key => $name): ?>
                        <option value="<?= $key; ?>"><?= ucwords(strtolower($name)); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="postalCode">Postal Code <sup>*</sup></label>
                <input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Postal Code">
            </div>
            <div class="form-group">
                <label for="phoneNo">Phone no. <sup>*</sup></label>
                <input type="tel" class="form-control" id="phoneNo" name="phoneNo" placeholder="Phone no.">
            </div>
            <div class="form-group">
                <label for="email">Email <sup>*</sup></label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
            </div>
            <div id="messages" class="alert hidden">
                <h3 id="status">Success</h3>
                <ul id="information" class="hidden list-unstyled">
                    <li id="user"><strong class="title">User ID: </strong><span class="value"></span></li>
                    <li id="password"><strong class="title">Password: </strong><span class="value"></span></li>
                    <li id="tracking"><strong class="title">Tracking Number: </strong><span class="value"></span></li>
                    <li id="link"><a target="_blank" class="value btn btn-default">Print now!</a></li>
                </ul>
                <ul id="list"></ul>
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit">Generate Label At Fedex</button>
            </div>
        </form>
    </div>

    <div id="success" class="hidden text-center">
        <div class="spinner-wrapper">
            <div class="spinner"></div>
            <h2>Generating your label...</h2>
        </div>
        <div class="info-block hidden">
            <i style="font-size: 150px; color: #43A047" class="glyphicon glyphicon-ok-sign"></i>
            <h2>Thank You!</h2>
            <p>Your submission has been received.</p>
            <p>Please check your email to print your FedEx shipping label.</p>
            <p>(It may take a couple  minutes)</p>
            <p class="fedex-link"><a href="#" id="_link">Go to Fedex</a></p>
        </div>
    </div>

    <script src="./resources/js/vendor.js"></script>
</body>
</html>