<?php
namespace Application;

require 'vendor/autoload.php';

header('Content-Type: application/json');

function jsonResult($data, $result = true) {
    return json_encode([
        "success" => $result,
        "data" => $data
    ]);
}

function getArrayValue($array, $index, $defaultValue = null) {
    if(isset($array[$index])) {
        return $array[$index];
    }

    return $defaultValue;
}

date_default_timezone_set("UTC");

use FedexShipping\Shipping\Parts\ApiDetails\ClientDetail;
use FedexShipping\Shipping\Parts\ApiDetails\TransactionDetail;
use FedexShipping\Shipping\Parts\ApiDetails\Version;
use FedexShipping\Shipping\Parts\ApiDetails\WebAuthenticationDetail;
use FedexShipping\Shipping\Parts\ShippingDetails\Address;
use FedexShipping\Shipping\Parts\ShippingDetails\Contact;
use FedexShipping\Shipping\Parts\ShippingDetails\Dimensions;
use FedexShipping\Shipping\Parts\ShippingDetails\DropoffType;
use FedexShipping\Shipping\Parts\ShippingDetails\Items;
use FedexShipping\Shipping\Parts\ShippingDetails\LabelSpecification;
use FedexShipping\Shipping\Parts\ShippingDetails\PackageLineItem;
use FedexShipping\Shipping\Parts\ShippingDetails\PackagingType;
use FedexShipping\Shipping\Parts\ShippingDetails\Payor;
use FedexShipping\Shipping\Parts\ShippingDetails\PendingShipmentDetail;
use FedexShipping\Shipping\Parts\ShippingDetails\Recipient;
use FedexShipping\Shipping\Parts\ShippingDetails\RecipientsSet;
use FedexShipping\Shipping\Parts\ShippingDetails\RequestedShipment;
use FedexShipping\Shipping\Parts\ShippingDetails\ResponsibleParty;
use FedexShipping\Shipping\Parts\ShippingDetails\ServiceType;
use FedexShipping\Shipping\Parts\ShippingDetails\ShipmentRecipient;
use FedexShipping\Shipping\Parts\ShippingDetails\ShipmentTimestamp;
use FedexShipping\Shipping\Parts\ShippingDetails\Shipper;
use FedexShipping\Shipping\Parts\ShippingDetails\ShippingChargesPayment;
use FedexShipping\Shipping\Parts\ShippingDetails\SpecialServicesRequested;
use FedexShipping\Shipping\Parts\ShippingDetails\TotalWeight;
use FedexShipping\Shipping\Request;
use FedexShipping\ShippingManager;

$contactName = getArrayValue($_POST, "contactName", '');
$company = getArrayValue($_POST, "company", '');
$addresses = getArrayValue($_POST, "address", []);
$city = getArrayValue($_POST, "city", '');
$state = getArrayValue($_POST, "state", '');
$postalCode = getArrayValue($_POST, "postalCode", '');
$phoneNo = getArrayValue($_POST, "phoneNo", '');
$email = getArrayValue($_POST, "email", '');

$config = require './config.php';

$manager = new ShippingManager('./OpenShipService_v7.wsdl');
$request = new Request();

$request->setWebAuthenticationDetail(
    WebAuthenticationDetail::create([
        "Key" => $config["Key"],
        "Password" => $config["Password"]
    ])
);

$request->setClientDetail(
    ClientDetail::create([
        "AccountNumber" => $config["AccountNumber"],
        "MeterNumber" => $config["MeterNumber"],
    ])
);

$request->setVersion(
    Version::create([
        'ServiceId' => 'ship',
        'Major' => '7',
        'Intermediate' => '0',
        'Minor' => '0',
    ])
);

// Resquested shipment
$requestedShipment = new RequestedShipment();

$requestedShipment->setDropOffType(DropoffType::create());
$requestedShipment->setShipTimestamp(ShipmentTimestamp::create());
$requestedShipment->setPackagingType(PackagingType::create());
$requestedShipment->setServiceType(ServiceType::create(ServiceType::STANDARD_OVERNIGHT));
$requestedShipment->setSpecialServicesRequested(SpecialServicesRequested::create([
    "PendingShipmentDetail" => PendingShipmentDetail::create([
        "Type" => PendingShipmentDetail::TYPE_EMAIL,
        "Message" => "Message",
        "Recipients" => RecipientsSet::create([
            ShipmentRecipient::create([
                "EmailAddress" => $email
            ])
        ])
    ]),
]));

$requestedShipment->setTotalWeight(TotalWeight::create([
    "Value" => $config["TotalWeight"]["Value"],
    "Unit" => $config["TotalWeight"]["Unit"]
]));

$requestedShipment->setRecipient(Recipient::create([
    "Contact" => Contact::create([
        'PersonName' => $config['Recipient']["Contact"]["PersonName"],
        'CompanyName' => $config['Recipient']["Contact"]['CompanyName'],
        'PhoneNumber' => $config['Recipient']["Contact"]['PhoneNumber']
    ]),
    "Address" => Address::create([
        'StreetLines' => $config['Recipient']["Address"]['StreetLines'],
        'City' => $config['Recipient']["Address"]['City'],
        'StateOrProvinceCode' => $config['Recipient']["Address"]['StateOrProvinceCode'],
        'PostalCode' => $config['Recipient']["Address"]['PostalCode'],
        'CountryCode' => $config['Recipient']["Address"]['CountryCode']
    ])
]));

$requestedShipment->setItems(Items::create([
    PackageLineItem::create([
        'SequenceNumber'=> $config['PackageDetails']["SequenceNumber"],
        'GroupPackageCount'=> $config['PackageDetails']["GroupPackageCount"],
        "ItemDescription" => "Case Materials",
        'Weight' => TotalWeight::create([
            'Value' => $config['PackageDetails']["Weight"]["Value"],
            'Unit' => $config['PackageDetails']["Weight"]["Unit"]
        ]),
        'Dimensions' => Dimensions::create([
            'Length' => $config['PackageDetails']["Dimensions"]["Length"],
            'Width' => $config['PackageDetails']["Dimensions"]["Width"],
            'Height' => $config['PackageDetails']["Dimensions"]["Height"],
            'Units' => $config['PackageDetails']["Dimensions"]["Units"],
        ])
    ]),
]));

$requestedShipment->setPackageCount(1);

$requestedShipment->setLabelSpecification(LabelSpecification::create([
    'LabelFormatType' => 'COMMON2D',
    'ImageType' => 'PDF',
    'LabelStockType' => 'PAPER_7X4.75'
]));

$requestedShipment->setShippingChargesPayment(ShippingChargesPayment::create([
    "Payor" => Payor::create([
        'ResponsibleParty' => ResponsibleParty::create([
            'AccountNumber' => $config["Payor"]['AccountNumber'],
            'Contact' => $config["Payor"]['Contact'],
            'Address' => $config["Payor"]['Address'],
        ])
    ]),
]));


// RECIPIENT
$requestedShipment->setShipper(Shipper::create([
    "Contact" => Contact::create([
        'PersonName' => $contactName,
        'CompanyName' => $company,
        'PhoneNumber' => $phoneNo
    ]),
    "Address" => Address::create([
        'StreetLines' => $addresses,
        'City' => $city,
        'StateOrProvinceCode' => $state,
        'PostalCode' => $postalCode,
        'CountryCode' => 'US',
        'Residential' => true
    ])
]));

$request->setRequestedShipment($requestedShipment);
$request->setTransactionDetail(
    TransactionDetail::create([
        "CustomerTransactionId" => uniqid("Rx"),
        "Information" => []
    ])
);

try {
    $response = $manager->sendRequest($request);

    if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
        echo jsonResult([
            "messages" => [],
            "information" => [
                'link' => $response->CompletedShipmentDetail->AccessDetail->AccessorDetails->EmailLabelUrl,
                'user' => $response->CompletedShipmentDetail->AccessDetail->AccessorDetails->UserId,
                'password' => $response->CompletedShipmentDetail->AccessDetail->AccessorDetails->Password,
                'tracking' => $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber,
            ],
        ], true);
    } else {
        $errors = count($response->Notifications) == 1 ? [$response->Notifications] : $response->Notifications;
        echo jsonResult([
            "errors" => $errors
        ], false);
    }
} catch(\SoapFault $e) {
    echo jsonResult([
        "errors" => [
            "Something wrong. Try again..."
        ]
    ], false);
}